import pandas as pd
# from pyomo.opt import SolverFactory
from pyomo.environ import *


class Optimize:

    def __init__(self, pv, demand, indirect_flex):
        self.pv = pv
        self.demand = demand
        self.indirect_flex = indirect_flex


    def _get_pyomo_timeseries(self, result, demand, production, dates, co2_grid, model,
                              keys=None, multi=True, data_set=None):
        """Custom function to retrieve optimization timeseries from `base.py`"""

        if keys == None:
            keys = ["grid_import", "grid_export", "battery_in", "battery_out",
                    "battery_energy", "charger_1", "charger_2", "charger_3", "charger_4"]

        for key in keys:
            df = pd.DataFrame(index=["_"], data=getattr(model, key).get_values())
            df = df.transpose()
            df.columns = [key]
            if multi:
                #             print(df)
                df = df.reset_index().drop(columns=["level_0", "level_1"])
            result[key] = df[key].tolist()

        result = pd.DataFrame(result)
        if "battery_in" in keys:
            result["battery_power"] = - (result["battery_in"] + result["battery_out"])

        if data_set != None:
            period = model.period.data()
            result["production"] = [data_set[i]["production"][t] for i in period
                                    for t in range(len(data_set[i]["production"]))]

            result["load"] = [data_set[i]["Consumption"][t] for i in period
                              for t in range(len(data_set[i]["Consumption"]))]

            result["dates"] = [data_set[i].index[t] for i in period
                               for t in range(len(data_set[i].index))]

        else:
            result["production"] = production
            result["co2_grid"] = co2_grid
            result["load"] = demand
            result["dates"] = dates

        return result

    def simulate_test(self, max_dis: float = -40, max_chg: float = 50, ch_1=0, ch_2=0, ch_3=0, ch_4=0,
                      ch_eff: float = 0.95, dch_eff: float = 0.95, max_soc: float = 50, sd=1e-6, min_soc: float = 10,
                      prodd: list = None, loadd: list = None, init_cap: float = 50, datess: list = None, timestep=60,
                      solver='gurobi', verbose=False, solver_path=None):

        # max_dis :  maximum discharge powwer value of the battery (should be negative)
        # max_chg : maximum charging power valu of the battery
        # ch_eff : Battery charghing efficiency (between 1 and 0)
        # dch_eff : Battery discharghing efficiency (between 1 and 0)
        # sd : self-discharge rate of the battery
        # init_cap: initial capacity of the battery
        # timestep : the timestep foor simulation in minutes
        # max_soc : Maximum allowed state of charge (0 <= max_soc <= capacity)
        # min_soc : Minimum allowed state of charge (0 <= min_soc <= capacity)
        # prodd : list containing the PV production values (PV production profile)
        # loadd : list containing the demand profile
        # period : number of hours for which the evaluation is being done (currently only considers 1 hour time step)
        # datess : currently unused
        # ch_1 : Energy consumed by charger 1 during the evaluation period
        # ch_2 : Energy consumed by charger 2 during the evaluation period
        # ch_3 : Energy consumed by charger 3 during the evaluation period
        # ch_4 : Energy consumed by charger 4 during the evaluation period
        # verbose: print out optimizer outputs
        # solver: LP solver to use for the optimization "gurobi" is default however change to "glpk" especially if using binder
        # returns a dataframe with all the calculated porameters

        # for data gaps , esentially relaxes a constraint, allowing for feasible solutions on days when there is no PV
        if sum(prodd) > 0:
            l_soc = 0.1
        else:
            l_soc = 0.03

        ############################################### Initializing Model
        m = ConcreteModel()
        timestep = timestep / 60


        m.ts = Set(initialize=list(range(0, len(prodd))), ordered=True)
        last = m.ts.last()

        m.grid_import = Var(m.ts, domain=NonNegativeReals)
        m.battery_in = Var(m.ts, domain=NegativeReals)
        m.battery_out = Var(m.ts, domain=NonNegativeReals)
        m.battery_energy = Var(m.ts, domain=NonNegativeReals, bounds=(min_soc, max_soc))
        m.battery_state = Var(m.ts, domain=Binary, bounds=(0, 1))
        m.grid_export = Var(m.ts, domain=NegativeReals)
        m.charger_1 = Var(m.ts, domain=NonNegativeReals, bounds=(0, 14))
        m.charger_2 = Var(m.ts, domain=NonNegativeReals, bounds=(0, 14))

        m.charger_3 = Var(m.ts, domain=NonNegativeReals, bounds=(0, 44))
        m.charger_4 = Var(m.ts, domain=NonNegativeReals, bounds=(0, 44))

        m.end_cap = Var(domain=NonNegativeReals, bounds=(min_soc, max_soc))

        #################################################### Param
        m.self_consumption = Var(domain=NonNegativeReals)  # ,bounds=(0,1))
        m.total_co2 = Var(domain=NonNegativeReals)

        #################################################### Rules
        # --------------------------------------------------------
        # ---------------------Charging----------------------------
        # --------------------------------------------------------

        def r_ev_charging_time(m, t):
            if datess[t].hour <= 7 or datess[t].hour > 20:
                return (m.charger_1[t] + m.charger_2[t] + m.charger_3[t] + m.charger_4[t] == 0)
            else:
                return Constraint.Skip

        def r_charger_1_total(m):
            return (sum(m.charger_1[t] for t in m.ts) == ch_1)

        def r_charger_2_total(m):
            return (sum(m.charger_2[t] for t in m.ts) == ch_2)

        def r_charger_3_total(m):
            return (sum(m.charger_3[t] for t in m.ts) == ch_3)

        def r_charger_4_total(m):
            return (sum(m.charger_4[t] for t in m.ts) == ch_4)

        # --------------------------------------------------------
        # ---------------------Battery----------------------------
        # --------------------------------------------------------

        def r_battery_max_powerin(m, t):
            return (m.battery_in[t] >= -max_chg * m.battery_state[t])

        def r_battery_max_powerout(m, t):
            return (m.battery_out[t] <= max_dis * (m.battery_state[t] - 1))

        def r_battery_energy(m, t):
            if t == 0:
                return m.battery_energy[t] == init_cap
            else:
                return (m.battery_energy[t - 1] * (1 - sd) - m.battery_energy[t] ==
                        m.battery_in[t - 1] * timestep * ch_eff + m.battery_out[t - 1] * timestep / dch_eff)

        def r_battery_min_energy(m, t):
            return (m.battery_energy[t] >= min_soc)

        def r_battery_max_energy(m, t):
            return (m.battery_energy[t] <= max_soc)

        def r_end_cap(m):
            return (m.battery_energy[last] * (1 - sd) - m.battery_in[last] * timestep * ch_eff - m.battery_out[
                last] * timestep / dch_eff
                    == m.end_cap)

        def r_battery_max_end_energy(m):
            return (m.end_cap <= max_soc)

        def r_battery_min_end_energy(m):
            return (m.end_cap >= min_soc * (1 + l_soc))

        # --------------------------------------------------------
        # ---------------------Energy Balance---------------------
        # --------------------------------------------------------

        def r_energy_balance(m, t):
            return (prodd[t] - (loadd[t] + m.charger_1[t] + m.charger_2[t] + m.charger_3[t] + m.charger_4[t])
                    + m.grid_export[t] + m.grid_import[t] + m.battery_in[t] + m.battery_out[t] == 0)

        # --------------------------------------------------------
        # ---------------------Grid-------------------------------
        # --------------------------------------------------------

        def r_grid_export(m, t):
            return (-m.grid_export[t] <= prodd[t])

        def r_grid_import(m, t):
            return (m.grid_import[t] <= loadd[t] + m.charger_1[t] + m.charger_2[t] + m.charger_3[t] + m.charger_4[t])

        def r_self_consumption(m):

            return (sum(m.grid_import[t] for t in m.ts) - sum(m.grid_export[t] for t in m.ts)
                    == m.self_consumption)


        # --------------------------------------------------------
        # ---------------------Add To Model-----------------------
        # --------------------------------------------------------
        ######### Battery ###############
        m.r1 = Constraint(m.ts, rule=r_battery_max_powerin)
        m.r2 = Constraint(m.ts, rule=r_battery_max_powerout)
        m.r3 = Constraint(m.ts, rule=r_battery_energy)
        m.r4 = Constraint(m.ts, rule=r_battery_min_energy)
        m.r5 = Constraint(m.ts, rule=r_battery_max_energy)
        m.r6 = Constraint(rule=r_battery_min_end_energy)
        m.r7 = Constraint(rule=r_battery_max_end_energy)
        m.r8 = Constraint(rule=r_end_cap)
        ######### Battery ###############
        m.r9 = Constraint(m.ts, rule=r_grid_export)
        m.r10 = Constraint(m.ts, rule=r_grid_import)

        ######### Battery ###############
        m.r11 = Constraint(m.ts, rule=r_ev_charging_time)
        m.r12 = Constraint(rule=r_charger_1_total)
        m.r13 = Constraint(rule=r_charger_2_total)
        m.r14 = Constraint(rule=r_charger_3_total)
        m.r15 = Constraint(rule=r_charger_4_total)

        ######### Energy Balance ###############
        m.r16 = Constraint(m.ts, rule=r_energy_balance)
        ######### Self-consumption ###############
        m.r17 = Constraint(rule=r_self_consumption)



        def objective_function(m):
            return m.self_consumption

        m.objective = Objective(rule=objective_function, sense=minimize)

        m.write("data/test.lp")
        with SolverFactory(solver, executable=solver_path) as opt:
            if solver == "glpk":
                opt.options['tmlim'] = 10
            results = opt.solve(m, tee=False)
            if verbose:
                print(results)
        return m, self._get_pyomo_timeseries(result=dict(), demand=loadd, production=prodd, co2_grid=None, dates=datess,
                                             multi=False,  model=m, keys=None)

    def simulate_cap(self, max_dis: float = 0.8, max_chg: float = 1, eff: float = 0.95, data=None, max_soc: float = 1.0,
                     sd=0, min_soc: float = 0.2, timestep=60, sc=0.71, solver='gurobi', verbose=False, solver_path=None):

        # Capacity: capacity of battery storage in KWh (could be in Wh but all parameters should be in W)
        # max_dis :  maximum discharge power as a fraction of battery capacity (range: 0-1)
        # max_chg : maximum charge power as a fraction of battery capacity (range: 0-1)
        # sd : self-discharge rate of the battery
        # init_cap: initial capacity of the battery
        # timestep : the timestep foor simulation in minutes
        # eff : Efficiency of the battery (between 1 and 0)
        # data: dataframe with the columns corresponding to the arguments passed for pv, demand and indirect_flex when initializing the class
        # max_soc : Maximum allowed state of charge (0 <= max_soc <= 1)
        # min_soc : Minimum allowed state of charge (0 <= min_soc <= 1)
        # timestep : the timestep of the timeseries data in minutes
        # sd :  rate of selfdischarge of the battery
        # data : timeseries dataframe containing the columns [production, Consumption, charger 1, charger 2, Taux de CO2]

        # returns a pyomo concretemodel

        ################### Model
        m = ConcreteModel()
        timestep = timestep / 60

        ##################################################### Sets
        m.ts = Set(initialize=list(range(0, len(data[0]))), ordered=True)
        m.period = Set(initialize=list(range(0, len(data))), ordered=True)
        last = m.ts.last()

        #################################################### chargers
        m.charger_1 = Var(m.period, m.ts, domain=NonNegativeReals, bounds=(0, 14))
        m.charger_2 = Var(m.period, m.ts, domain=NonNegativeReals, bounds=(0, 14))
        m.charger_3 = Var(m.period, m.ts, domain=NonNegativeReals, bounds=(0, 44))
        m.charger_4 = Var(m.period, m.ts, domain=NonNegativeReals, bounds=(0, 44))
        #################################################### grid
        m.grid_import = Var(m.period, m.ts, domain=NonNegativeReals)
        m.grid_export = Var(m.period, m.ts, domain=NegativeReals)
        #################################################### Battery
        m.battery_energy = Var(m.period, m.ts, domain=NonNegativeReals)
        m.battery = Var(m.period, m.ts, domain=Reals)
        m.end_cap = Var(m.period, domain=NonNegativeReals)
        #################################################### Param
        m.self_consumption = Var(domain=NonNegativeReals)  # ,bounds=(0,1))
        m.total_co2 = Var(domain=NonNegativeReals)
        m.capacity = Var(domain=NonNegativeReals)


        #################################################### Rules
        # --------------------------------------------------------
        # ---------------------Chargers----------------------------
        # --------------------------------------------------------

        def r_ev_charging_time(m, p, t):
            if data[p].index[t].hour <= 8 or data[p].index[t].hour > 20:  # needs work
                return (m.charger_1[p, t] + m.charger_2[p, t] + m.charger_3[p, t] + m.charger_4[p, t] == 0)
            else:
                return Constraint.Skip

        def r_charger_1_total(m, p):
            return (sum(m.charger_1[p, t] for t in m.ts) == data[p][self.indirect_flex[0]].sum())

        def r_charger_2_total(m, p):
            return (sum(m.charger_2[p, t] for t in m.ts) == data[p][self.indirect_flex[1]].sum())

        def r_charger_3_total(m, p):
            return (sum(m.charger_3[p, t] for t in m.ts) == data[p][self.indirect_flex[2]].sum())

        def r_charger_4_total(m, p):
            return (sum(m.charger_4[p, t] for t in m.ts) == data[p][self.indirect_flex[3]].sum())

        # --------------------------------------------------------
        # ---------------------Battery----------------------------
        # --------------------------------------------------------

        def r_battery_max_powerin(m, p, t):
            return (m.battery[p, t] <= max_chg * m.capacity)

        def r_battery_max_powerout(m, p, t):
            return (m.battery[p, t] >= -max_dis * m.capacity)

        def r_battery_energy(m, p, t):
            if t == 0 and p == 0:
                return (m.battery_energy[p, t] == m.battery_energy[m.period.last(), last])

            elif t == 0 and p > 0:
                return (m.battery_energy[p, t] == m.end_cap[p - 1])  # needs work
            else:
                return (m.battery_energy[p, t - 1] * (1 - sd) - m.battery_energy[p, t]
                        == -m.battery[p, t - 1] * timestep * eff)

        def r_battery_min_energy(m, p, t):
            return (m.battery_energy[p, t] >= min_soc * m.capacity)

        def r_battery_max_energy(m, p, t):
            return (m.battery_energy[p, t] <= max_soc * m.capacity)

        def r_end_cap(m, p):
            return (m.battery_energy[p, last] * (1 - sd) + m.battery[p, last] * timestep * eff
                    == m.end_cap[p])

        def r_battery_max_end_energy(m, p):
            return (m.end_cap[p] <= max_soc * m.capacity)

        def r_battery_min_end_energy(m, p):
            return (m.end_cap[p] >= min_soc * 1.001 * m.capacity)

        # --------------------------------------------------------
        # ------------------Grid ---------------------------------
        # --------------------------------------------------------

        def r_grid_export(m, p, t):
            return (-m.grid_export[p, t] <= data[p][self.pv][t])

        def r_grid_import(m, p, t):
            return (m.grid_import[p, t] <= data[p][self.demand][t] + m.charger_1[p, t] + m.charger_2[p, t] +
                    m.charger_3[p, t] + m.charger_4[p, t])

        # --------------------------------------------------------
        # ------------------Energy Balance-----------------------
        # --------------------------------------------------------
        def r_energy_balance(m, p, t):
            return (data[p][self.pv][t] - (data[p][self.demand][t] + m.charger_1[p, t] + m.charger_2[p, t] +
                                           m.charger_3[p, t] + m.charger_4[p, t]) + m.grid_export[p, t] + m.grid_import[
                        p, t] - m.battery[p, t] == 0)

        def r_calc_sc(m):
            return (sum(
                sum((data[p][self.demand][t] + m.charger_1[p, t] + m.charger_2[p, t] + m.charger_3[p, t] + m.charger_4[
                    p, t])
                    - m.grid_import[p, t] for t in m.ts) for p in m.period) / sum(sum(data[p][self.pv][t]
                                                                                      for t in m.ts) for p in
                                                                                  m.period) == m.self_consumption)

        def r_self_consumption_(m):
            return (m.self_consumption == sc)

        # --------------------------------------------------------
        # ---------------------Add To Model-----------------------
        # --------------------------------------------------------
        ######### Battery ###############
        m.r1 = Constraint(m.period, m.ts, rule=r_battery_max_powerin)
        m.r2 = Constraint(m.period, m.ts, rule=r_battery_max_powerout)
        m.r3 = Constraint(m.period, m.ts, rule=r_battery_energy)
        m.r4 = Constraint(m.period, m.ts, rule=r_battery_min_energy)
        m.r5 = Constraint(m.period, m.ts, rule=r_battery_max_energy)
        m.r6 = Constraint(m.period, rule=r_battery_min_end_energy)
        m.r7 = Constraint(m.period, rule=r_battery_max_end_energy)
        m.r8 = Constraint(m.period, rule=r_end_cap)
        ######### Grid ###############

        m.r9 = Constraint(m.period, m.ts, rule=r_grid_export)

        m.r10 = Constraint(m.period, m.ts, rule=r_grid_import)

        ######### Charging ###############
        m.r11 = Constraint(m.period, rule=r_charger_1_total)
        m.r12 = Constraint(m.period, rule=r_charger_2_total)
        m.r13 = Constraint(m.period, rule=r_charger_3_total)
        m.r14 = Constraint(m.period, rule=r_charger_4_total)
        m.r15 = Constraint(m.period, m.ts, rule=r_ev_charging_time)

        ######### Self-consumption ###############
        m.r16 = Constraint(rule=r_self_consumption_)
        m.r17 = Constraint(rule=r_calc_sc)

        ######### Energy Balance ###############
        m.r18 = Constraint(m.period, m.ts, rule=r_energy_balance)



        def objective_function(m):
            return (m.capacity)

        m.objective = Objective(rule=objective_function, sense=minimize)

        m.write("data/Capacity_sizing.lp")
        with SolverFactory(solver, executable=solver_path) as opt:
            results = opt.solve(m, tee=False)
            if verbose:
                print(results)

        keys = ["grid_import", "grid_export", "battery", "charger_1", "charger_2", "charger_3", "charger_4",
                "battery_energy"]
        df = self._get_pyomo_timeseries(result=dict(), demand=None, production=None, co2_grid=None,
                                        dates=None, multi=True, data_set=data, model=m, keys=keys)
        return m, df
