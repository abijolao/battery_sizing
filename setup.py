import setuptools
from setuptools import setup

with open("requirements.txt", "r") as f:
    requirements = f.read().split()

setup(
    name="battery_sizing",
    version="0.1",
    description="battery sizing optimization taking into account indirect flexibility",
    url="none",
    author="Nana Kofi",
    author_email=["nana-kofi.twum_duah@grenoble-inp.fr"],
    keywords="Optimization, indirect flexibility, PV, battery,  Electric vehicles",
    packages=setuptools.find_packages(),
    # install_requires=requirements,
    python_requires=">=3.7.12",
)
