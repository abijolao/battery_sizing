# Indirect flexibility for sizing energy systems sizing

Abstract: This article proposes a method for taking into account the indirect flexibility (charging of Electric Vehicles) available in buildings for the sizing of battery storage systems (direct flexibility). A Linear Programming approach was applied to data sourced from the Predis-MHI platform (a living lab) such that the day-to-day charging of EVs, as well as the scheduling of the charging and discharging of the proposed battery, were optimized whilst simultaneously dimensioning the battery capacity. Our results indicate that based on the percentage increase in self-consumption with reference to the base case, it is possible to reduce the battery capacity required by up to 100% as compared to a methodology that doesn’t account for the indirect flexibility. Whilst relevant, the sizing approach proposed in this article assumes optimal human behavior, which is usually difficult to achieve. Our proposed approach can be adapted and used for the dimensioning of direct flexibilities for both residential and commercial/public buildings.

Keywords: Behavioural response, Greenhouse gas emissions, Optimisation, Self-sufficiency, Energy communities, Energy sufficiency.

# Participate
- Install `battery_sizing` using `pip install -e .` in the root folder.
- Add your data file ensuring that it has a column for:
    - PV production
    - Demand (withouth the indirect flexibility)
    - Indirect flexibility
- Propose new models in `battery_sizing/optimization`.
`
